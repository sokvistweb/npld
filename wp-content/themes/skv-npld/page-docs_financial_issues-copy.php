<?php /* Template Name: Docs Financial Issues Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>
				
				<?php echo do_shortcode("[downloads category='financial-issues' per_page=-1 loop_start='<ul class=doc-download>' loop_end='</ul>']"); ?>
				
		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
    
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->
    
    

<?php get_footer(); ?>