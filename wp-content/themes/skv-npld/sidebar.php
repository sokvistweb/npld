<div id="sidebar">
    <div class="inner">

        <!-- Login -->
        <section class="login-top">
            <ul class="actions vertical">
                <?php if (is_user_logged_in()) : ?>
                <li><a href="<?php echo wp_logout_url( home_url() ); ?>" class="button fit">Logout</a></li>
                <?php else : ?>
                <li><a href="<?php bloginfo('home') ?>/members-login" class="button fit">Members Log in</a></li>
                <?php endif;?>
            </ul>
        </section>

        <!-- Menu -->
        <?php
        $menu_name = 'header-menu';
        $locations = get_nav_menu_locations();
        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
        $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
        ?>
        <nav id="menu" role="navigation">
            <ul>
                <?php
                $count = 0;
                $submenu = false;
                foreach( $menuitems as $item ):
                    $link = $item->url;
                    $title = $item->title;
                    $class = $item->classes[0];
                    // item does not have a parent so menu_item_parent equals 0 (false)
                    if ( !$item->menu_item_parent ):
                    // save this id for later comparison with sub-menu items
                    $parent_id = $item->ID;
                    $parent_name = $item->title;
                ?>

                <li class="">
                    <a href="<?php echo $link; ?>" class="<?php echo $class; ?>">
                        <?php echo $title; ?>
                    </a>
                <?php endif; ?>

                    <?php if ( $parent_id == $item->menu_item_parent ): ?>

                        <?php if ( !$submenu ): $submenu = true; ?>
                        <span class="opener"><?php echo $parent_name; ?></span>
                        <ul class="sub-menu">
                        <?php endif; ?>

                            <li class="">
                                <a href="<?php echo $link; ?>" class=""><?php echo $title; ?></a>
                            </li>

                        <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                        </ul>
                        <?php $submenu = false; endif; ?>

                    <?php endif; ?>

                <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                </li>                           
                <?php $submenu = false; endif; ?>

            <?php $count++; endforeach; ?>

            </ul>
        </nav>


        <!-- Section
            <div class="mini-posts">
                <article>
                    <a href="http://www.npld.eu/melt/" class="image" title="Visit MELT website" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/melt-logo.png" alt="" /></a>
                </article>
            </div>
        </section> -->

        <!-- Search -->
        <section id="search">
            <form class="search" method="get" action="<?php echo esc_url( home_url() ); ?>">
                <div role="search">
                    <input class="search-input" type="search" name="s" aria-label="Search site for:" placeholder="<?php esc_html_e( 'To search, type and hit enter.', 'html5blank' ); ?>">
                    <!--<button class="search-submit" type="submit"><?php esc_html_e( 'Search', 'html5blank' ); ?></button>-->
                </div>
            </form>
            <div id="google_translate_element"></div><script type="text/javascript">
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
            }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </section>

        <!-- Section -->
        <section class="alt">
            <nav id="sub-menu">
                <ul>
                    <li><a class="legal" href="<?php bloginfo('home') ?>/legal-notice" title="Legal notice">Legal notice</a></li>
                    <li><a class="legal" href="<?php bloginfo('home') ?>/cookies-policy" title="Cookies Policy">Cookies Policy</a></li>
                    <li><a class="legal" href="<?php bloginfo('home') ?>/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
                    <li><a href="<?php bloginfo('home') ?>/wp-admin" title="Admin site page">Admin</a></li>
                </ul>
            </nav>
            <p class="copyright"><span>&copy; NPLD 2018</span>Design: <a href="https://sokvist.com">Sokvist</a></p>
        </section>

    </div><!-- /.inner -->
</div>