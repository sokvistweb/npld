<?php /* Template Name: Our Publications */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>
        
        <div class="row divider-flex posts posts-publications">
            
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'publications', 'posts_per_page' => 16, 'paged' => $paged )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="w12u equal-h">
                <article>
                    <div class="box-style">
                        <h2><?php the_title(); ?></h2>
                        
                        <?php if( get_field('number') || get_field('date') ): ?>
                        <div class="extra-fields"><strong><?php the_field('number'); ?></strong> <?php the_field('date'); ?>
                        <i>
                        <?php $posttags = get_the_tags();
                        if ($posttags) {
                            
                            echo "Tags: ";
                            
                            foreach($posttags as $tag) {
                                echo "<span>$tag->name</span>";
                            }
                        } ?>
                        </i>
                        </div>
                        <?php endif; ?>
                        
                        <?php if( get_field('authorship') ): ?>
                            <p class="authorship"><i>Authorship: <?php the_field('authorship'); ?></i></p>
                        <?php endif; ?>
                        
                        <?php the_content(); ?>
                        
                        <div>
                        <?php if( get_field('document') ): ?>
                            <a href="<?php the_field('document'); ?>" class="dl-link" target="_blank">Download file <span class="meta-nav">↓</span>
                            </a>
                        <?php endif; ?>
                        </div>
                    </div>
                </article>
            </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
        
        <?php wp_numeric_posts_nav(); ?>
        
    </section>
    <!-- /Content -->


<?php get_footer(); ?>