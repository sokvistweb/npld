<?php /* Template Name: Page Campus */ get_header('campus'); ?>
    
    
<!-- Content -->
<section>
    <header class="main">
        <h1><?php the_title(); ?></h1>
    </header>

    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
    
        <span class="image fit">
            <!-- page thumbnail -->
            <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                <?php the_post_thumbnail('single-post'); // Fullsize image for the single post. ?>
            <?php endif; ?>
            <!-- /page thumbnail -->
        </span>

        <?php the_content(); ?>

    <?php endwhile; ?>

    <?php else : ?>

            <h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

    <?php endif; ?>

</section>
<!-- /Content -->


<section>

    <div class="row">
        <div class="column">
            <!-- Dispaly Custom Post Type (html5-blank in this case) -->
            <?php query_posts('post_type=speakers'); while (have_posts ()): the_post(); ?>
            
            <h3><?php the_title() ?></h3>
            
            <?php the_content() ?>
            
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">link</a>
            
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>

</section>


<?php get_footer('campus'); ?>