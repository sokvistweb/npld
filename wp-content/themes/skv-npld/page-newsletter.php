<?php /* Template Name: Newsletter Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
    
    
        <div class="mc-newsletters">
           <!-- How to:
           https://mailchimp.com/help/add-an-email-campaign-archive-to-your-website/
           https://mailchimp.com/help/organize-campaigns-into-folders/
           -->
            <script language="javascript" src="//npld.us16.list-manage.com/generate-js/?u=6cb9df3f869de87960f5bd3bd&fid=8339&show=60" type="text/javascript"></script>
        </div>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>

