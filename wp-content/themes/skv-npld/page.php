<?php get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
        
            <span class="image fit">
                <!-- page thumbnail -->
                <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                    <?php the_post_thumbnail('single-post'); // Fullsize image for the single post. ?>
                <?php endif; ?>
                <!-- /page thumbnail -->
            </span>

            <?php the_content(); ?>

		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>