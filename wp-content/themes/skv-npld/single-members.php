<?php get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

        <div class="card-full">
            <div class="card-row">
                <div class="card-col">
                    <p>Name of organisation</p>
                </div>
                <div class="card-col with-img">
                    <p><?php the_field('name_of_organisation'); ?></p>
                    <!-- logo -->
                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <?php the_post_thumbnail('large'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Country</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('country'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Type of Organisation</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('type_of_organisation'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Short note on organisation background</p>
                </div>
                <div class="card-col">
                    <?php the_field('short_note'); ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Website</p>
                </div>
                <div class="card-col">
                    <p><a href="<?php the_field('website'); ?>" title="<?php the_field('website'); ?>" target="_blank"><?php the_field('website'); ?></a></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Contact</p>
                </div>
                <div class="card-col">
                    <?php the_field('contact'); ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Address</p>
                </div>
                <div class="card-col">
                    <?php the_field('address'); ?>
                </div>
            </div>
        </div>
        
        <a href="<?php bloginfo('home') ?>/our-members" class="button icon fa-arrow-circle-left">Back to All Members</a>

		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>