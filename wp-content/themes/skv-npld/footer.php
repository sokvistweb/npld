            <div class="eupopup-container eupopup-container-bottom">
                <div class="eupopup-markup">
                    <div class="eupopup-head">This website is using cookies</div>
                    <div class="eupopup-body">We use cookies to ensure that we give you the best experience on our website. By using this website, you agree to this.</div>
                    <div class="eupopup-buttons"> 
                        <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                        <a href="<?php bloginfo('home') ?>/cookies-policy" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a>
                    </div>
                    <div class="clearfix"></div>
                    <a href="#" class="eupopup-closebutton">
                        <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                    </a>
                </div>
            </div>
            
            <footer id="footer">
                
                <!-- Section -->
                <div class="getintouch">
                    <header class="major">
                        <h2>Get in touch</h2>
                    </header>
                    <ul class="contact">
                        <li class="fa-envelope-o"><a href="mailto:npld@npld.eu" title="Get in contact by email" target="_blank">npld@npld.eu</a></li>
                        <li class="fa-phone"><a href="tel:0032496286342">+32 (0) 496 286 342/3</a></li>
                        <li class="fa-home">Rue de la Pépinière | Boomkwekerijstraat 1, 3.<br />
                        B-1000 Brussels (Belgium)</li>
                    </ul>
                </div>

                <!-- MailChimp -->
                <div id="mc_embed_signup" class="subscribe-form">
                    <header class="major">
                        <h2>Subcribe to our newsletter</h2>
                    </header>
                    <form action="https://npld.us16.list-manage.com/subscribe/post?u=6cb9df3f869de87960f5bd3bd&amp;id=bc34d01952" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response"></div>
                                <div class="response" id="mce-success-response"></div>
                            </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div class="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value="">
                            </div>
                            <div class="clear">
                            <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                        </div>
                    </form>
                </div>

            </footer>

        </div><!-- /.inner -->
    </div><!-- /Main -->


    <!-- Sidebar -->
    <?php get_sidebar(); ?>
    <!-- /Sidebar -->

    </div><!-- /Wrapper -->


    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>

    <?php wp_footer(); ?>

    <!-- analytics
    <script>
    (function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
    (f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
    l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
    ga('send', 'pageview');
    </script> -->

	</body>
</html>
