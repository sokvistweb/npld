<?php get_header(); ?>
    
    
    <!-- Content -->
    <section class="blog-single">
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <div class="row">
            <div class="w12u">
               <?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
               
                <!-- post details -->
                <div class="expiration-date">
                    <p class="due-date"><i><?php echo do_shortcode("[postexpirator type='date']"); ?></i></p>
                </div>
                <!-- /post details -->
               
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <span class="image fit">
                        <!-- post thumbnail -->
                        <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                            <?php the_post_thumbnail('single-post'); // Fullsize image for the single post. ?>
                        <?php endif; ?>
                        <!-- /post thumbnail -->
                    </span>

                    <?php the_content(); // Dynamic Content. ?>

                </article>
                
                <?php endwhile; ?>
                <?php else : ?>

                <article>
                    <h1><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
                </article>

	        <?php endif; ?>
            </div>
        </div>
        
        <div class="row">
            <div class="w12u">
                <?php ja_prev_next_post_nav(); ?>
            </div>
        </div>

    </section>
    <!-- /Content -->
    
    
    <section>
        <header class="major">
            <h2 class="h2-upcoming">More Upcoming Events</h2>
        </header>
        <div class="row divider-flex posts posts-upcoming">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'our-event', 'ourevents-categories' => 'upcoming-events', 'posts_per_page' => 2 )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <h3><?php the_title(); ?></h3>
                        <p class="due-date"><i>Event due on <strong><?php echo do_shortcode("[postexpirator type='date']"); ?></strong></i></p>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>

        </div>
    </section>
    
 
<?php get_footer(); ?>
