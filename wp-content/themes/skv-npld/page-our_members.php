<?php /* Template Name: Our Members Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
        <div class="row">
            <div class="w12u">
                <article>
                
                <span class="image fit">
                    <!-- page thumbnail -->
                    <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                        <?php the_post_thumbnail('single-post'); // Fullsize image for the single post. ?>
                    <?php endif; ?>
                    <!-- /page thumbnail -->
                </span>

				<?php the_content(); ?>
				
				</article>
            </div>
        </div>
        
        <?php endwhile; ?>
		<?php else : ?>
                
        <div class="row">
            <div class="w12u">
                <article>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
				
				</article>
            </div>
        </div>

		<?php endif; ?>
        
        <?php wp_reset_query(); ?>
        
        <div class="row">
           
            <div class="w6u">
               <h2>Full Members</h2>
               
                <ul class="alt docs">
                    <?php if (have_posts()) : ?>
                    <?php query_posts(array(
                        'post_type' => 'members',
                        'members-categories' => 'full-members',
                        'posts_per_page' => 99,
                        'orderby' => 'title',
                        'order' => 'ASC'
                    )); ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <li class="members"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?><span><?php the_field('represented_language'); ?></span></a></li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
            

            <div class="w6u">
                <h2>Associate Members</h2>
               
                <ul class="alt docs">
                    <?php if (have_posts()) : ?>
                    <?php query_posts(array(
                        'post_type' => 'members',
                        'members-categories' => 'associate-members',
                        'posts_per_page' => 99,
                        'orderby' => 'title',
                        'order' => 'ASC'
                    )); ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <li class="members"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?><span><?php the_field('represented_language'); ?></span></a></li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        

    </section>
    <!-- /Content -->


<?php get_footer(); ?>