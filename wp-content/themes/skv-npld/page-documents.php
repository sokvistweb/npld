<?php /* Template Name: Documents Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
            <?php custom_breadcrumbs(); ?>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

            <?php 
            // Restrict Page to Require a Login
            // https://martech.zone/login-content-wordpress-page-template/
            global $user_ID;  wp_get_current_user(); ?>
            <?php if($user_ID) { ?>
            
            <?php the_content(); ?>
            
            
            <!-- listing folders
            https://www.advancedcustomfields.com/resources/post-object/ -->
            <?php
            // vars
            $folders = get_field('folders');
            // check
            if( $folders ): ?>
                <ul class="doc-download">
                <?php foreach( $folders as $folder): ?>
                    <li>
                        <a class="download-link filetype-icon filetype-fol" href="<?php echo get_permalink($folder->ID); ?>"><?php echo get_the_title($folder->ID); ?></a>
                    </li>
                <?php endforeach; ?>
                </ul>
            <?php endif; 
            ?>
            
            
            <!-- listing files by category -->
            <?php echo do_shortcode("[downloads category='".get_field('category_slug')."' per_page=-1 orderby='title' order='ASC' loop_start='<ul class=doc-download>' loop_end='</ul>']"); ?>
            
            
            <?php } else { ?>
            <h2>Members Only</h2>
            <p>We're sorry, the content you are trying to reach is restricted to members only.</p>
            <?php } ?>
				
		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
        <?php wp_reset_query(); ?>
   
   
        <?php get_template_part( 'templates/content', 'login' ); ?>
        
    
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->
    
    

<?php get_footer(); ?>