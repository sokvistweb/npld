<?php /* Template Name: Memphis Docs Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

            <?php 
            // Restrict Page to Require a Login
            // https://martech.zone/login-content-wordpress-page-template/
            global $user_ID;  wp_get_current_user(); ?>
            <?php if($user_ID) { ?>
            
            <?php the_content(); ?>
            
            
            <?php } else { ?>
            <h2>Members Only</h2>
            <p>We're sorry, the content you are trying to reach is restricted to members only.</p>
            <?php } ?>
				
		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
        <?php wp_reset_query(); ?>
   
   
        <?php get_template_part( 'templates/content', 'login' ); ?>
        
    
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>