<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-21"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-47544981-21');
    </script>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/favicon.ico" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/campus/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700|Open+Sans:400,400i,600" rel="stylesheet">
    
    <!-- Open Graph -->
    <meta property="og:locale" content="en-GB">
    <meta property="og:type" content="website">
    <meta property="og:title" content="NPLD – Network to Promote Linguistic Diversity">
    <meta property="og:description" content="A European wide network working in the field of language policy & planning for Constitutional, Regional and Small-State Languages (CRSS) across Europe">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/og-image.jpg">
    <meta property="og:url" content="https://www.npld.eu/">
    <meta property="og:site_name" content="NPLD">
    
    <!-- Twitter Card  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@NPLDeu">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="https://www.npld.eu/">
    <meta name="twitter:title" content="NPLD – Network to Promote Linguistic Diversity">
    <meta name="twitter:description" content="A European wide network working in the field of language policy & planning for Constitutional, Regional and Small-State Languages (CRSS) across Europe">
    <meta name="twitter:image" content="assets/images/og-image.jpg">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
    
    <div class="eupopup eupopup-top"></div>
    
    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>" class="NPLD logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/npld-logo.svg" alt="NPLD logo" width="268" height="150" /></a>
                    <ul class="icons">
                        <li>££</li>
                        <li><a href="https://twitter.com/NPLDeu" title="Follow us on Twitter" target="_blank" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="https://www.facebook.com/NPLDeu/" NPLD Facebook page target="_blank" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="mailto:npld@npld.eu" title="Get in contact by email" target="_blank" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                    </ul>
                </header>
                
