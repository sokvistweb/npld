<?php get_header(); ?>

	<!-- Content -->
    <section>
        <header class="main">
			<h1><?php echo sprintf( __( '%s Search Results for: ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
		</header>
		<!-- /section -->
		
		<!-- search -->
<form class="search" method="get" action="<?php echo esc_url( home_url() ); ?>">
	<div role="search">
		<input class="search-input" type="search" name="s" aria-label="Search site for:" value="<?php echo sprintf( __( '', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?>">
		<button class="search-submit small" type="submit"><?php esc_html_e( 'Search', 'html5blank' ); ?></button>
	</div>
</form>
<!-- /search -->
		
		<div class="row divider-flex posts">
            
            <ul class="search-results">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <li>
                    <h2><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php the_excerpt(); ?></p>
                </li>
            <?php endwhile; ?>
            <?php else : ?>
				<li><p><?php esc_html_e( 'Sorry, no results for your search.', 'html5blank' ); ?></p></li>
            <?php endif; ?>
            </ul>
        </div>
        
        <?php wp_numeric_posts_nav(); ?>
		
	</section>
    <!-- /Content -->

<?php get_footer(); ?>
