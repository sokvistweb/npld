<?php
/**
 * Default output for a download via the [download] shortcode
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** @var DLM_Download $dlm_download */
?>

<a class="download-link filetype-icon <?php echo 'filetype-' . $dlm_download->get_version()->get_filetype(); ?>"
   title="Download file" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow">
   
	<!--<span><?php echo $dlm_download->get_version()->get_filename(); ?></span>-->
	
	<span><?php $dlm_download->the_title(); ?></span>
	
	<span>(<i class="fa fa-download"></i> <?php printf( _n( '1', '%d', $dlm_download->get_download_count(), 'download-monitor' ), $dlm_download->get_download_count() ) ?>)</span>
	
	<span><?php echo get_the_date(); ?></span>

</a>
