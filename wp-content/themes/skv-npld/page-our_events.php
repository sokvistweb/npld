<?php /* Template Name: Our Events Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>
        
        <h2 class="h2-upcoming">Upcoming Events</h2>

        <div class="row divider-flex posts posts-upcoming">
            
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'our-event', 'ourevents-categories' => 'upcoming-events', 'posts_per_page' => 8, 'paged' => $paged )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <h3><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                        <p class="due-date">
                            <i>Event due on <strong><?php echo do_shortcode("[postexpirator type='date']"); ?></strong></i>
                            
                            <i><?php if( get_field('location') ): ?> 
                            in <strong><?php the_field('location'); ?></strong>
                            <?php endif; ?></i>
                        </p>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
        
        <h2>Past Events</h2>
        <div class="row divider-flex posts posts-past">
            
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'our-event', 'ourevents-categories' => 'past-events', 'posts_per_page' => 8, 'paged' => $paged )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <h3><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                        <p class="due-date">
                            <i>Event held on <strong><?php echo do_shortcode("[postexpirator type='date']"); ?></strong></i>
                            
                            <i><?php if( get_field('location') ): ?> 
                            in <strong><?php the_field('location'); ?></strong>
                            <?php endif; ?></i>
                        </p>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
        
        <?php wp_numeric_posts_nav(); ?>
        
    </section>
    <!-- /Content -->


<?php get_footer(); ?>