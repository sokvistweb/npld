<?php /* Template Name: Login Page SKV */ get_header(); ?>
    
    
	<!-- Content -->
    <section class="skv-login-form">
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>
        
        
        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
        <?php wp_reset_query(); ?>
        
        
        <?php 
        // https://ahmadawais.com/create-front-end-login-page-wordpress/
            global $user_login;
            // In case of a login error.
            if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
    	            <div class="aa_error">
    		            <p><?php _e( 'FAILED: Try again!', 'SKV' ); ?></p>
    	            </div>
            <?php 
                endif;
            // If user is already logged in.
            if ( is_user_logged_in() ) : ?>

                <div class="skv_logout"> 
                    
                    <h2><?php _e( 'Hello ', 'SKV' ); echo $user_login; ?></h2>
                    
                    <p><?php _e( 'You are already logged in.', 'SKV' ); ?></p>

                </div>

                <ul class="actions">
                   <li><a id="wp-submit" class="button special small" href="<?php echo wp_logout_url(); ?>" title="Logout">
                    <?php _e( 'Logout', 'SKV' ); ?></a></li>
                </ul>

            <?php 
                // If user is not logged in.
                else: 
                
                    // Login form arguments.
                    $args = array(
                        'echo'           => true,
                        'redirect'       => home_url(), 
                        'form_id'        => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in'   => __( 'Log In' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
                        'remember'       => false,
                        'value_username' => NULL,
                        'value_remember' => true
                    ); 
                    
                    // Calling the login form.
                    wp_login_form( $args );
                endif;
        ?> 

	</section>
	<!-- /Content -->


<?php get_footer(); ?>