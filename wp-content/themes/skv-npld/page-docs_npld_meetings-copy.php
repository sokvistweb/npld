<?php /* Template Name: Docs NPLD Meetings Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

            <?php 
            // Restrict Page to Require a Login
            // https://martech.zone/login-content-wordpress-page-template/
            global $user_ID; get_currentuserinfo(); ?>
            <?php if($user_ID) { ?>
            
            <?php the_content(); ?>
            
            


<?php 
if ( is_category() ) {
$this_category = get_category($cat);
if($this_category->category_parent):
else:
   $this_category = wp_list_categories('orderby=id&depth=5&show_count=0&title_li=&use_desc_for_title=1&child_of='.$this_category->cat_ID."&echo=0");
echo '<ul>'. $this_category . '</ul>';
endif;
} 
?>





            
            <?php echo do_shortcode("[downloads category='npld-meetings' per_page=-1 loop_start='<ul class=doc-download>' loop_end='</ul>']"); ?>
				
            <?php } else { ?>
            <h2>Members Only</h2>
            <p>We're sorry, the content you are trying to reach is restricted to members only.</p>
            <?php } ?>
				
		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
        <?php wp_reset_query(); ?>
   
   
        <?php get_template_part( 'templates/content', 'login' ); ?>
        
    
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->
    
    

<?php get_footer(); ?>