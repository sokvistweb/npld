<?php /* Template Name: Documents Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

            <?php 
            // Restrict Page to Require a Login
            // https://martech.zone/login-content-wordpress-page-template/
            global $user_ID; get_currentuserinfo(); ?>
            <?php if($user_ID) { ?>
            
            <?php the_content(); ?>
            
            <!--<?php the_field('folder_1'); ?>-->
            
            
            
            
            <?php 
            // vars
            $folders = get_field('folder_1');
            // check 
            if( $folders ): ?>
            <ul>
                <?php foreach( $folders as $folder ): ?>
                <li>
                    <a href="<?php echo $folder ?>"><?php echo $folder; ?></a>
                </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
            
            
            
            <?php
            
            $post_objects = get_field('folder_no_2');

            if( $post_objects ): ?>
                <ul>
                <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                <?php endforeach; ?>
                </ul>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif;

            ?>
            
            
            
            <?php
            // vars
            $folders = get_field('folder_no_2');
            // check
            if( $folders ): ?>
                <ul>
                <?php foreach( $folders as $folder): ?>
                    <li>
                        <a href="<?php echo get_permalink($folder->ID); ?>"><?php echo get_the_title($folder->ID); ?></a>
                    </li>
                <?php endforeach; ?>
                </ul>
            <?php endif;

            ?>
            
            
            
            
            
            <?php echo do_shortcode("[downloads category='".get_field('category_slug')."' per_page=-1 loop_start='<ul class=doc-download>' loop_end='</ul>']"); ?>
				
            <?php } else { ?>
            <h2>Members Only</h2>
            <p>We're sorry, the content you are trying to reach is restricted to members only.</p>
            <?php } ?>
				
		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>
        <?php wp_reset_query(); ?>
   
   
        <?php get_template_part( 'templates/content', 'login' ); ?>
        
    
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->
    
    

<?php get_footer(); ?>