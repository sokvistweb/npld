<?php /* Template Name: Our Languages Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>
		
		<?php get_template_part( 'templates/content', 'imappage' ); ?>
        <?php get_template_part( 'templates/content', 'mappage' ); ?>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>