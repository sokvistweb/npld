<?php /* Template Name: Language Single Page */ get_header(); ?>
    
    
<!-- Content -->
<section>
    <header class="main">
        <h1><?php the_title(); ?></h1>
    </header>

    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

    <div class="card-full">
        <?php if( get_field('name_of_the_language_title') || get_field('name_of_the_language') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('name_of_the_language_title'); ?>
            </div>
            <div class="card-col with-img">
                <?php the_field('name_of_the_language'); ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if( get_field('number_of_total_speakers_title') || get_field('number_of_total_speakers') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('number_of_total_speakers_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('number_of_total_speakers'); ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if( get_field('number_of_native_spakers_title') || get_field('number_of_native_spakers') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('number_of_native_spakers_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('number_of_native_spakers'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('regulated_by_title') || get_field('regulated_by') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('regulated_by_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('regulated_by'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('geographical_area_title') || get_field('geographical_area') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('geographical_area_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('geographical_area'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('language_family_title') || get_field('language_family') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('language_family_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('language_family'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('status_of_the_language_title') || get_field('status_of_the_language') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('status_of_the_language_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('status_of_the_language'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('status_at_the_university_title') || get_field('status_at_the_university') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('status_at_the_university_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('status_at_the_university'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('fields_of_use_title') || get_field('fields_of_use') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('fields_of_use_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('fields_of_use'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('entity_member_title') || get_field('entity_member') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('entity_member_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('entity_member'); ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if( get_field('year_of_source_title') || get_field('year_of_source') ): ?>
        <div class="card-row">
            <div class="card-col">
                <?php the_field('year_of_source_title'); ?>
            </div>
            <div class="card-col">
                <?php the_field('year_of_source'); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>

    <a href="<?php bloginfo('home') ?>/our-languages" class="button icon fa-arrow-circle-left">Back to All Languages</a>

    <?php endwhile; ?>

    <?php else : ?>

            <h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

    <?php endif; ?>

</section>
<!-- /Content -->


<?php get_footer(); ?>