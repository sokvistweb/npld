<?php get_header(); ?>

	
	<!-- Content -->
    <section>
        <header class="main">
            <h1>News</h1>
        </header>

        <div class="row divider-flex posts">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail('medium_large'); ?></a>
                        <?php endif; ?>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->
	

<?php get_footer(); ?>
