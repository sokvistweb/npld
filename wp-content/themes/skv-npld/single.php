<?php get_header(); ?>
    
    
    <!-- Content -->
    <section class="blog-single">
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <div class="row">
            <div class="w12u">
               <?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
               
                <!-- post details -->
                <div class="post-details">
                    <span class="date">
                        <time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_date(); ?>
                        </time>
                    </span>
                    <span class="category"><?php esc_html_e( 'Categorised in: ', 'html5blank' ); the_category( ', ' ); // Separated by commas. ?></span>
                </div>
                <!-- /post details -->
               
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <span class="image fit">
                        <!-- post thumbnail -->
                        <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                            <?php the_post_thumbnail('single-post'); // Fullsize image for the single post. ?>
                        <?php endif; ?>
                        <!-- /post thumbnail -->
                    </span>

                    <?php the_content(); // Dynamic Content. ?>

                </article>
                
                <?php endwhile; ?>
                <?php else : ?>

                <article>
                    <h1><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
                </article>

	        <?php endif; ?>
            </div>
        </div>
        
        <div class="row">
            <div class="w12u">
                <?php ja_prev_next_post_nav(); ?>
            </div>
        </div>

    </section>
    <!-- /Content -->
    
    
    <section>
        <header class="major">
            <h2>More News</h2>
        </header>
        <div class="row divider-flex posts">
            <?php $the_query = new WP_Query( 'posts_per_page=2' ); ?>
            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </section>
    
 
<?php get_footer(); ?>
