<?php get_header(); ?>

   
    <?php get_template_part( 'templates/content', 'imap' ); ?>
    <?php get_template_part( 'templates/content', 'map' ); ?>


    <!-- Banner -->
    <section id="banner">
        <?php if (have_posts()) : ?>
        <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'homepage' )); ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="content">
            <header>
                <h1><?php the_field('title_home'); ?></h1>
            </header>
            
            <?php the_content(); ?>
            
            <ul class="actions">
                <li><a href="<?php the_field('link_to_page'); ?>" class="button big"><?php the_field('link_content'); ?></a></li>
            </ul>
        </div>
        <span class="image object">
            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
            <?php the_post_thumbnail('large'); ?>
            <?php endif; ?>
        </span>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </section>


	<?php if ( have_rows( 'banner' ) ) : ?>
    <?php while ( have_rows( 'banner' ) ) : the_row(); ?>
	<!-- Section -->
    <section>
        <div class="banner-featured">
			<a class="banner-link" href="<?php the_sub_field( 'link' ); ?>" title="<?php the_sub_field( 'title' ); ?>" target="_blank">
                <?php 
                $image = get_sub_field('image');
                if( $image ):
                // Image variables.
                $url = $image['url'];
                $alt = $image['alt'];
                // Thumbnail size attributes.
                $size = 'single-post';
                $thumb = $image['sizes'][ $size ];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ];
                ?>
                <img src="<?php echo esc_url($thumb); ?>" loading="lazy" alt="<?php echo esc_attr($alt); ?>" width="<?php echo esc_attr($width); ?>" height="<?php echo esc_attr($height); ?>" />
                <?php endif; ?>
            </a>
		</div>
	</section>
	<?php endwhile; ?>
	<?php endif; ?>


    <!-- Section -->
    <section>
        <div class="features">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'our-members' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <article class="three-cols">
                    <div class="content">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </article>
            </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'our-publications' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <article class="three-cols">
                    <div class="content">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </article>
            </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'resources-on-linguistic-diversity-in-europe' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <article class="three-cols">
                    <div class="content">
                        <h3><?php the_field('short-title'); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </article>
            </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </section>


    <!-- Section -->
    <section>
        <header class="major">
            <h2>NPLD News</h2>
        </header>

        <div class="row divider-flex posts">
            <?php $the_query = new WP_Query( 'posts_per_page=2' ); ?>
            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail('medium_large'); ?></a>
                        <?php endif; ?>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </section>
                    

<?php get_footer(); ?>
