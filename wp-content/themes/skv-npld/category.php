<?php get_header(); ?>

	
	<!-- Content -->
    <section>
        <header class="main">
            <h1><?php esc_html_e( 'Category: ', 'html5blank' ); single_cat_title(); ?></h1>
        </header>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <article id="eo-venue-cat">
            <div class="box-style">
                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                <p><?php the_excerpt(); ?></p>
                <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">Read more</a></div>
            </div>
        </article>
        <?php endwhile; ?>
        <?php endif; ?>
        
        <?php wp_numeric_posts_nav(); ?>

    </section>
    <!-- /Content -->

<?php get_footer(); ?>
