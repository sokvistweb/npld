<?php /* Template Name: Language Single Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

        <div class="card-full">
            <div class="card-row">
                <div class="card-col">
                    <p>Name of the language</p>
                </div>
                <div class="card-col with-img">
                    <p><?php the_field('name_of_the_language'); ?></p>
                    <!-- logo -->
                    <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                        <?php the_post_thumbnail('large'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Number of total speakers</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('number_of_total_speakers'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Number of native speakers</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('number_of_native_spakers'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Regulated by</p>
                </div>
                <div class="card-col">
                    <?php the_field('regulated_by'); ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Geographical area</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('geographical_area'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Language family</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('language_family'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Status of the language</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('status_of_the_language'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Status at the University</p>
                </div>
                <div class="card-col">
                    <?php the_field('status_at_the_university'); ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Fields of use (administration, education, media, leisure)</p>
                </div>
                <div class="card-col">
                    <?php the_field('fields_of_use'); ?>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Entity member of the NPLD</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('entity_member'); ?></p>
                </div>
            </div>
            <div class="card-row">
                <div class="card-col">
                    <p>Year of source</p>
                </div>
                <div class="card-col">
                    <p><?php the_field('year_of_source'); ?></p>
                </div>
            </div>
        </div>
        
        <a href="<?php bloginfo('home') ?>/our-languages" class="button icon fa-arrow-circle-left">Back to All Languages</a>

		<?php endwhile; ?>

		<?php else : ?>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		<?php endif; ?>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>