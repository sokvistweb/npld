<?php get_header(); ?>

   
    <?php get_template_part( 'templates/content', 'imap' ); ?>
    <?php get_template_part( 'templates/content', 'map' ); ?>


    <!-- Banner -->
    <section id="banner">
        <?php if (have_posts()) : ?>
        <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'homepage' )); ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="content">
            <header>
                <h1><?php the_field('title_home'); ?></h1>
            </header>

        </div>
        <div class="image object">
            
            <?php the_content(); ?>
            
            <ul class="actions">
                <li><a href="<?php the_field('link_to_page'); ?>" class="button big"><?php the_field('link_content'); ?></a></li>
            </ul>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </section>


    <!-- Section -->
    <section>
        <div class="features">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'our-members' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <article class="three-cols">
                    <span class="icon"><img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/th-members.jpg" alt="" width="150" height="150" /></span>
                    <div class="content">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </article>
            </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'advocacy-actions' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <article class="three-cols">
                    <span class="icon"><img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/th-advocacy.jpg" alt="" width="150" height="150" /></span>
                    <div class="content">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </article>
            </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'resources-on-linguistic-diversity-in-europe' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <article class="three-cols">
                    <span class="icon"><img class="svg-icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/th-resources.jpg" alt="" width="150" height="150" /></span>
                    <div class="content">
                        <h3><?php the_field('short-title'); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </article>
            </a>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </section>


    <!-- Section -->
    <section>
        <header class="major">
            <h2>NPLD News</h2>
        </header>

        <div class="row divider-flex posts">
            <?php $the_query = new WP_Query( 'posts_per_page=2' ); ?>
            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
            <div class="w6u equal-h">
                <article>
                    <div class="box-style">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail('large'); ?></a>
                        <?php endif; ?>
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <div><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="button special">Read more</a></div>
                    </div>
                </article>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </section>
                    

<?php get_footer(); ?>
