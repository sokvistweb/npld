<!-- Section -->
<section class="map">
    <div class="simple-map">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/europe-map-simple.svg">
        <div class="map-button">
            <a class="button" href="<?php bloginfo('home') ?>/our-languages" title="Our Languages page">Our languages</a>
        </div>
    </div>
</section>