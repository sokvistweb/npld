<?php 
// https://ahmadawais.com/create-front-end-login-page-wordpress/
    global $user_login;
    // In case of a login error.
    if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>

<div class="aa_error">
    <p>
        <?php _e( 'FAILED: Try again!', 'SKV' ); ?>
    </p>
</div>

<?php 
    endif;
    // If user is already logged in.
    if ( is_user_logged_in() ) :
    // do nothing
?>

<?php 
    // If user is not logged in.
    else: 

        // Login form arguments.
        $args = array(
            'echo'           => true,
            'redirect'       => site_url( $_SERVER['REQUEST_URI'] ), 
            'form_id'        => 'loginform',
            'label_username' => __( 'Username' ),
            'label_password' => __( 'Password' ),
            'label_remember' => __( 'Remember Me' ),
            'label_log_in'   => __( 'Log In' ),
            'id_username'    => 'user_login',
            'id_password'    => 'user_pass',
            'id_remember'    => 'rememberme',
            'id_submit'      => 'wp-submit',
            'remember'       => false,
            'value_username' => NULL,
            'value_remember' => true
        ); 

        // Calling the login form.
        wp_login_form( $args );
    endif;
?>