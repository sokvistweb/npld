<!-- Section -->
<section class="map">
    <div class="simple-map">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/europe-map-simple.svg">
        <ul>
            <?php $exclude = get_page_by_title('Ladin, Mocheno & Cimbrian'); ?>
            <?php if (have_posts()) : ?>
            <?php query_posts(array(
                'post_type' => 'page',
                'post_parent' => 17,
                'posts_per_page' => 99,
                'post__not_in' => array($exclude->ID)
            )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <li id="<?php echo( basename(get_permalink()) ); ?>" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2><?php the_title(); ?></h2>
                        <p><?php the_field('geographical_area_button'); ?></p>
                        <a class="button special small" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">More info</a>
                    </div>
                </div>
            </li>
            <?php endwhile; ?>
		    <?php endif; ?>
       
           <?php wp_reset_query(); ?>

            <?php if (have_posts()) : ?>
            <?php query_posts(array(
                'post_type' => 'page',
                'post_parent' => 17,
                'name' => 'ladin-mocheno-cimbrian'
            )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <li id="<?php echo( basename(get_permalink()) ); ?>" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <?php the_field('geographical_area_button'); ?>
                    </div>
                </div>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
        </ul>
    </div>
    <!-- /end of simple-map -->
</section>