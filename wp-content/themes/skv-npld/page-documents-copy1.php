<?php /* Template Name: Documents Page */ get_header(); ?>
    
    
    <!-- Content -->
    <section>
        <header class="main">
            <h1><?php the_title(); ?></h1>
        </header>

        <ul class="docs alt">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'download-monitor', 'posts_per_page' => 10, 'paged' => $paged )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <li class="cat-item cat-item-1">
                <a class="download-link filetype-icon <?php echo 'filetype-' . $dlm_download->get_version()->get_filetype(); ?>"
                   title="<?php if ( $dlm_download->get_version()->has_version_number() ) {
                       printf( __( 'Version %s', 'download-monitor' ), $dlm_download->get_version()->get_version_number() );
                   } ?>" href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow">
                    <?php echo $dlm_download->get_version()->get_filename(); ?>
                    (<?php printf( _n( '1 download', '%d downloads', $dlm_download->get_download_count(), 'download-monitor' ), $dlm_download->get_download_count() ) ?>)
                </a>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </ul>

    </section>
    <!-- /Content -->


<?php get_footer(); ?>