<?php get_header(); ?>


<main role="main" id="maincontent">
    
    <section class="container">
        
        <h2 class="title"><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h2>
        
        <div class="row">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <div class="column card-column">
                <div class="card">
                    <div class="card-image">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail('medium'); ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="card-header">
                        <div class="card-title"><h2><?php the_title(); ?></h2></div>
                    </div>
                    <div class="card-body"><?php the_excerpt(); ?></div>
                    <div class="card-footer"><a class="button" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">Read more</a></div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        
        <?php wp_numeric_posts_nav(); ?>
        
    </section>
    
</main>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
