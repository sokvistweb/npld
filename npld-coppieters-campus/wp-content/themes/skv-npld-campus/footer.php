

		<footer id="footer">
			<div class="container">
				<ul class="copyright">
					<li>&copy; <?php echo esc_html( date( 'Y' ) ); ?> <?php bloginfo( 'name' ); ?></li>
					<li><a class="legal" href="https://www.npld.eu/legal-notice" title="Legal notice">Legal notice</a></li>
					<li><a class="legal" href="https://www.npld.eu/cookies-policy" title="Cookies Policy">Cookies Policy</a></li>
					<li><a class="legal" href="https://www.npld.eu/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
					<li>Design: <a href="https://sokvist.com">Sokvist</a></li>
				</ul>
			</div>
		</footer>

	
	</div><!-- /Wrapper -->


    <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/vendor/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/min/main.min.js"></script>

    <?php wp_footer(); ?>

    <!--
    <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
    </script>
    <script src="https://www.google-analytics.com/analytics.js" async></script>
    -->

	</body>
</html>
