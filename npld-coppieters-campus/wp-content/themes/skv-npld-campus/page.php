<?php get_header(); ?>


<main role="main" id="maincontent" class="wrapper">
    
    <section class="container">
        <!-- This is the page title (any page using Default templagte, as this is page.php template) -->
        <h2 class="title"><?php the_title(); ?></h2>
        
        <div class="row">
            <div class="column">
                <!-- The Loop - Check if there are posts -->
                <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>
                <!-- Dispaly the content of the current post -->
                <?php the_content(); ?>
                
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>  
        
    </section>
    
</main>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
