<?php get_header(); ?>


<main role="main" class="wrapper">
    
    <section class="container" id="post-404">
        
        <h2 class="title"><?php esc_html_e( 'Page not found', 'html5blank' ); ?></h2>
        
        <div class="row">
            <div class="column">
                <h2>
					<a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return home?', 'html5blank' ); ?></a>
				</h2>
            </div>
        </div>  
        
    </section>
    
</main>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
