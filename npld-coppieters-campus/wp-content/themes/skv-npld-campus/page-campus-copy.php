<?php /* Template Name: Campus Page Template */ get_header(); ?>


<body <?php body_class('is-preload'); ?>>
	
	<!-- Header -->
	<section id="header">
		<header>
			<a href="<?php echo esc_url( home_url() ); ?>" rel="home" title="Home page | <?php bloginfo( 'name' ); ?>" class="home-page">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/campus-npld-coppieters-logo-2.png" alt="NPLD-Coppieters Campus logo" />
			</a>
		</header>
		<nav id="nav">
			<ul>
				<li><a href="#title" class="active">-</a></li>
				<?php if( get_field('about_section_menu_name') ): ?>
				<li><a href="#one"><?php the_field( 'about_section_menu_name' ); ?></a></li>
				<?php endif; ?>
				<?php if( get_field('programme_section_menu_name') ): ?>
				<li><a href="#two"><?php the_field( 'programme_section_menu_name' ); ?></a></li>
				<?php endif; ?>
				<?php if( get_field('speakers_section_menu_name') ): ?>
				<li><a href="#three"><?php the_field( 'speakers_section_menu_name' ); ?></a></li>
				<?php endif; ?>
				<?php if( get_field('editions_section_menu_name') ): ?>
				<li><a href="#four"><?php the_field( 'editions_section_menu_name' ); ?></a></li>
				<?php endif; ?>
				<?php if( get_field('contact_section_menu_name') ): ?>
				<li><a href="#five"><?php the_field( 'contact_section_menu_name' ); ?></a></li>
				<?php endif; ?>
			</ul>
		</nav>
		<footer>
			<ul class="icons">
				<?php if( get_field('linkedin', 32) ): ?>
				<li><a href="<?php the_field( 'linkedin', 32 ); ?>" class="icon brands fa-linkedin" target="_blank"><span class="label">Linkedin</span></a></li>
				<?php endif; ?>
				<?php if( get_field('facebook', 32) ): ?>
				<li><a href="<?php the_field( 'facebook', 32 ); ?>" class="icon brands fa-facebook-f" target="_blank"><span class="label">Facebook</span></a></li>
				<?php endif; ?>
				<?php if( get_field('instagram', 32) ): ?>
				<li><a href="<?php the_field( 'instagram', 32 ); ?>" class="icon brands fa-instagram" target="_blank"><span class="label">Instagram</span></a></li>
				<?php endif; ?>
				<?php if( get_field('youtube', 32) ): ?>
				<li><a href="<?php the_field( 'youtube', 32 ); ?>" class="icon brands fa-youtube" target="_blank"><span class="label">YouTube</span></a></li>
				<?php endif; ?>
				<?php if( get_field('twitter', 32) ): ?>
				<li><a href="<?php the_field( 'twitter', 32 ); ?>" class="icon brands fa-twitter" target="_blank"><span class="label">Twitter</span></a></li>
				<?php endif; ?>
				<?php if( get_field('email', 32) ): ?>
				<li><a href="mailto:<?php the_field( 'email', 32 ); ?>" class="icon solid fa-envelope"><span class="label"><?php the_field( 'email' ); ?></span></a></li>
				<?php endif; ?>
			</ul>
		</footer>
	</section>
	
	
	<!-- Wrapper -->
	<div id="wrapper">

		<!-- Main -->
		<main id="main" role="main">
			
			<!-- Title -->
			<?php if ( have_rows( 'main_section' ) ) : ?>
			<?php while ( have_rows( 'main_section' ) ) : the_row(); ?>
			<section id="title">
				<div class="container">
					<header>
						<h1 id="logo"><?php the_title(); ?></h1>
						<h2><?php the_sub_field( 'tagline' ); ?></h2>
						<p><?php the_sub_field( 'place_and_date' ); ?></p>
					</header>

					<?php the_sub_field( 'main_content' ); ?>

				</div>
			</section>
			<?php endwhile; ?>
			<?php endif; ?>
			
			
			
			<!-- One -->
			<?php if ( have_rows( 'about_section' ) ) : ?>
			<?php while ( have_rows( 'about_section' ) ) : the_row(); ?>
			<?php if( get_sub_field('section_title') || get_sub_field('section_content') ): ?>
			<section id="one">
				<div class="container">

					<div class="major">
						<h2><?php the_sub_field( 'section_title' ); ?></h2>
					</div>
					
					<?php the_sub_field( 'section_content' ); ?>
					
				</div>
			</section>
			<?php endif; ?>
			<?php endwhile; ?>
			<?php endif; ?>
			
			
			
			<!-- Two -->
			<?php if ( have_rows( 'programme_section' ) ) : ?>
			<?php while ( have_rows( 'programme_section' ) ) : the_row(); ?>
			<?php if( get_sub_field('section_title') ): ?>
			<section id="two">
				<div class="container">
					<?php if( get_sub_field('section_title') ): ?>
					<h2><?php the_sub_field( 'section_title' ); ?></h2>
					<?php endif; ?>
					
					<?php if( get_sub_field('section_title') ): ?>
					<?php the_sub_field( 'section_content' ); ?>
					<?php endif; ?>
					<!-- Day 1 -->
					<?php if( get_sub_field('title_day_1') ): ?>
					<h3><?php the_sub_field( 'title_day_1' ); ?></h3>
					<?php endif; ?>
					
					<ul class="feature-icons">
					<?php if ( have_rows( 'activity_1' ) ) : ?>
					<?php while ( have_rows( 'activity_1' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php if ( have_rows( 'activity_2' ) ) : ?>
					<?php while ( have_rows( 'activity_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_3' ) ) : ?>
					<?php while ( have_rows( 'activity_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_4' ) ) : ?>
					<?php while ( have_rows( 'activity_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_5' ) ) : ?>
					<?php while ( have_rows( 'activity_5' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_6' ) ) : ?>
					<?php while ( have_rows( 'activity_6' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					</ul>
					
					<!-- Day 2 -->
					<?php if( get_sub_field('title_day_2') ): ?>
					<h3><?php the_sub_field( 'title_day_2' ); ?></h3>
					<?php endif; ?>
					
					<?php if( get_sub_field('title_day_2') ): // hide ul if no title ?>
					<ul class="feature-icons">
					<?php if ( have_rows( 'activity_1_2' ) ) : ?>
					<?php while ( have_rows( 'activity_1_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php if ( have_rows( 'activity_2_2' ) ) : ?>
					<?php while ( have_rows( 'activity_2_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_3_2' ) ) : ?>
					<?php while ( have_rows( 'activity_3_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_4_2' ) ) : ?>
					<?php while ( have_rows( 'activity_4_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_5_2' ) ) : ?>
					<?php while ( have_rows( 'activity_5_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_6_2' ) ) : ?>
					<?php while ( have_rows( 'activity_6_2' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					</ul>
					<?php endif; ?>
					
					<!-- Day 3 -->
					<?php if( get_sub_field('title_day_3') ): ?>
					<h3><?php the_sub_field( 'title_day_3' ); ?></h3>
					<?php endif; ?>
					
					<?php if( get_sub_field('title_day_3') ): // hide ul if no title ?>
					<ul class="feature-icons">
					<?php if ( have_rows( 'activity_1_3' ) ) : ?>
					<?php while ( have_rows( 'activity_1_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php if ( have_rows( 'activity_2_3' ) ) : ?>
					<?php while ( have_rows( 'activity_2_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_3_3' ) ) : ?>
					<?php while ( have_rows( 'activity_3_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_4_3' ) ) : ?>
					<?php while ( have_rows( 'activity_4_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_5_3' ) ) : ?>
					<?php while ( have_rows( 'activity_5_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_6_3' ) ) : ?>
					<?php while ( have_rows( 'activity_6_3' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					</ul>
					<?php endif; ?>
					
					<!-- Day 4 -->
					<?php if( get_sub_field('title_day_4') ): ?>
					<h3><?php the_sub_field( 'title_day_4' ); ?></h3>
					<?php endif; ?>
					
					<?php if( get_sub_field('title_day_4') ): // hide ul if no title ?>
					<ul class="feature-icons">
					<?php if ( have_rows( 'activity_1_4' ) ) : ?>
					<?php while ( have_rows( 'activity_1_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php if ( have_rows( 'activity_2_4' ) ) : ?>
					<?php while ( have_rows( 'activity_2_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_3_4' ) ) : ?>
					<?php while ( have_rows( 'activity_3_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_4_4' ) ) : ?>
					<?php while ( have_rows( 'activity_4_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_5_4' ) ) : ?>
					<?php while ( have_rows( 'activity_5_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
						<?php if ( have_rows( 'activity_6_4' ) ) : ?>
					<?php while ( have_rows( 'activity_6_4' ) ) : the_row(); ?>
						<?php if( get_sub_field('icon') || get_sub_field('time') || get_sub_field('event') ): ?>
						<li class="icon solid fa-<?php the_sub_field( 'icon' ); ?>"><strong><?php the_sub_field( 'time' ); ?></strong> <?php the_sub_field( 'event' ); ?></li>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php endif; ?>
					</ul>
					<?php endif; ?>

				</div>
			</section>
			<?php endif; ?>
			<?php endwhile; ?>
			<?php endif; ?>
			
			
			
			<!-- Three -->
			<?php if ( have_rows( 'speakers_section' ) ) : ?>
			<?php while ( have_rows( 'speakers_section' ) ) : the_row(); ?>
			<?php if( get_sub_field('section_title') ): ?>
			<section id="three">
				<div class="container">
					
					<h2><?php the_sub_field( 'section_title' ); ?></h2>
					
					<?php the_sub_field( 'section_content' ); ?>
					
					<?php $speaker = get_sub_field( 'speaker' ); ?>
					<?php if ( $speaker ) : ?>
					<div class="features">
						<?php foreach ( $speaker as $post ) : ?>
						<article>
							<a href="#" class="image">
								<?php the_post_thumbnail('custom-size'); ?>
							</a>
							<div class="inner">
								<h3><?php the_title(); ?></h3>
								<div>
								<?php the_content(); ?>
								</div>
							</div>
						</article>
						<?php endforeach; ?>
					</div>
					<?php 
					// Reset the global post object so that the rest of the page works correctly.
					wp_reset_postdata(); ?>
					<?php endif; ?>
					
				</div>
			</section>
			<?php endif; ?>
			<?php endwhile; ?>
			<?php endif; ?>
			
			
			
			<!-- Four -->
			<?php if ( have_rows( 'editions_section' ) ) : ?>
			<?php while ( have_rows( 'editions_section' ) ) : the_row(); ?>
			<section id="four">
				<div class="container">

					<h2><?php the_sub_field( 'section_title' ); ?></h2>

					<?php if (have_posts()) : ?>
					<?php query_posts(array( 
						'post_type' => 'page',
						'post__not_in' => array(get_the_ID()),
						'meta_key' => '_wp_page_template',
						'meta_value' => 'page-campus.php' 
					)); 
					?>
					<?php while (have_posts()) : the_post(); ?>


					<a href="<?php the_permalink(); ?>" class="past-editions">
						<h3 id="logo"><?php the_title(); ?></h3>
						<h4><?php the_sub_field( 'tagline' ); ?></h4>
						<p><?php the_sub_field( 'place_and_date' ); ?></p>
					</a>

					<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); ?>

				</div>
			</section>
			<?php endwhile; ?>
			<?php endif; ?>
			
			
			
			<!-- Five -->
			<?php if ( have_rows( 'contact_section' ) ) : ?>
			<?php while ( have_rows( 'contact_section' ) ) : the_row(); ?>
			<?php if( get_sub_field('section_title') ): ?>
			<section id="five">
				<div class="container">
					<h2><?php the_sub_field( 'section_title' ); ?></h2>
					
					<?php the_sub_field( 'section_content' ); ?>
					
				</div>
			</section>
			<?php endif; ?>
			<?php endwhile; ?>
			<?php endif; ?>
			
			
		
		</main>


<?php get_footer(); ?>
