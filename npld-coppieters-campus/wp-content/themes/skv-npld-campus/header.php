<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
        
        <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="author" content="">
        <meta name="description" content="<?php bloginfo( 'description' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Use Favic-o-matic to generate all the favicons and touch icons you need: https://favicomatic.com/ -->
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo esc_url( get_template_directory_uri() ); ?>/android-icon-192x192.png">
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon.png" />
        <link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon-precomposed.png">
        
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/css/style.css">

        <!-- Links to information about the author(s) of the document -->
        <link rel="author" href="humans.txt">

        <!-- Facebook (and some others) use the Open Graph protocol: see http://ogp.me/ for details -->
        <!--<meta property="fb:app_id" content="123456789">
        <meta property="og:url" content="https://example.com/">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Content Title">
        <meta property="og:image" content="assets/images/og-image.jpg">
        <meta property="og:image:alt" content="A description of what is in the image (not a caption)">
        <meta property="og:description" content="Description Here">
        <meta property="og:site_name" content="Site Name">
        <meta property="og:locale" content="en_UK">
        <meta property="article:author" content="">-->

        <!-- Twitter: see https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/summary for details -->
        <!--<meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@site_account">
        <meta name="twitter:creator" content="@individual_account">
        <meta name="twitter:url" content="https://example.com/">
        <meta name="twitter:title" content="Content Title">
        <meta name="twitter:description" content="Content description less than 200 characters">
        <meta name="twitter:image" content="assets/images/og-image.jpg">
        <meta name="twitter:image:alt" content="A text description of the image conveying the essential nature of an image to users who are visually impaired. Maximum 420 characters.">-->

		<?php wp_head(); ?>

	</head>
