<?php 
$page = 'page logged-in';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section>
                        <header class="main">
                            <h1>Calendar of activities</h1>
                        </header>

                        
                        <div class="widget-calendar">
                        <div id="eo_calendar_widget-2" class="widget widget_calendar eo_widget_calendar style6">
                            <div id="eo_calendar_widget-2_content" class="eo-widget-cal-wrap" data-eo-widget-cal-id="eo_calendar_widget-2">
                                <table id="wp-calendar"><caption> marzo 2018 </caption>
                                    
                                    <thead><tr><th title="lunes" scope="col">L</th><th title="martes" scope="col">M</th><th title="miércoles" scope="col">X</th><th title="jueves" scope="col">J</th><th title="viernes" scope="col">V</th><th title="sábado" scope="col">S</th><th title="domingo" scope="col">D</th></tr>
                                    </thead>
                                    
                                    <tbody><tr><td class="pad eo-before-month" colspan="1">&nbsp;</td><td class="pad eo-before-month" colspan="1">&nbsp;</td><td class="pad eo-before-month" colspan="1">&nbsp;</td><td data-eo-wc-date="2018-03-01" class="eo-past-date"> 1 </td><td data-eo-wc-date="2018-03-02" class="eo-past-date"> 2 </td><td data-eo-wc-date="2018-03-03" class="eo-past-date"> 3 </td><td data-eo-wc-date="2018-03-04" class="eo-past-date"> 4 </td></tr><tr><td data-eo-wc-date="2018-03-05" class="eo-past-date"> 5 </td><td data-eo-wc-date="2018-03-06" class="eo-past-date"> 6 </td><td data-eo-wc-date="2018-03-07" class="eo-past-date"> 7 </td><td data-eo-wc-date="2018-03-08" class="eo-past-date"> 8 </td><td data-eo-wc-date="2018-03-09" class="eo-past-date"> 9 </td><td data-eo-wc-date="2018-03-10" class="eo-past-date"> 10 </td><td data-eo-wc-date="2018-03-11" class="eo-past-date"> 11 </td></tr><tr><td data-eo-wc-date="2018-03-12" class="eo-past-date"> 12 </td><td data-eo-wc-date="2018-03-13" class="eo-past-date"> 13 </td><td data-eo-wc-date="2018-03-14" class="eo-past-date"> 14 </td><td data-eo-wc-date="2018-03-15" class="eo-past-date"> 15 </td><td data-eo-wc-date="2018-03-16" class="eo-past-date"> 16 </td><td data-eo-wc-date="2018-03-17" class="eo-past-date"> 17 </td><td data-eo-wc-date="2018-03-18" class="eo-past-date"> 18 </td></tr><tr><td data-eo-wc-date="2018-03-19" class="eo-past-date"> 19 </td><td data-eo-wc-date="2018-03-20" class="eo-past-date"> 20 </td><td data-eo-wc-date="2018-03-21" class="eo-past-date event eo-event-venue-yerbabuena-espai eo-event-cat-consulta eo-event-tag-consulta eo-event-tag-estado-de-animo eo-event-tag-psicologia eo-event-tag-salud-mental eo-event-tag-salud-natural eo-event-running eo-multi-day"> <a title="Psicólogo" href="http://yerbabuena/eventos/evento/on/2018/03/21"> 21 </a></td><td data-eo-wc-date="2018-03-22" class="eo-past-date"> 22 </td><td data-eo-wc-date="2018-03-23" class="eo-past-date"> 23 </td><td data-eo-wc-date="2018-03-24" class="eo-past-date"> 24 </td><td data-eo-wc-date="2018-03-25" class="eo-past-date"> 25 </td></tr><tr><td data-eo-wc-date="2018-03-26" class="eo-past-date"> 26 </td><td data-eo-wc-date="2018-03-27" class="eo-past-date"> 27 </td><td data-eo-wc-date="2018-03-28" class="eo-past-date event eo-event-venue-yerbabuena-espai eo-event-cat-consulta eo-event-tag-consulta eo-event-tag-estado-de-animo eo-event-tag-psicologia eo-event-tag-salud-mental eo-event-tag-salud-natural eo-event-running eo-multi-day"> <a title="Psicólogo" href="http://yerbabuena/eventos/evento/on/2018/03/28"> 28 </a></td><td data-eo-wc-date="2018-03-29" class="eo-past-date event eo-event-venue-yerbabuena-espai eo-event-cat-taller eo-event-running eo-multi-day"> <a title="SURYA SOUL" href="http://yerbabuena/eventos/evento/on/2018/03/29"> 29 </a></td><td data-eo-wc-date="2018-03-30" class="eo-past-date"> 30 </td><td data-eo-wc-date="2018-03-31" class="eo-past-date"> 31 </td><td class="pad eo-after-month" colspan="1">&nbsp;</td></tr></tbody>
                                    
                                    <tfoot>
                                        <tr><td id="eo-widget-prev-month" colspan="3"><a title="Mes pasado" href="http://yerbabuena?eo_month=2018-02">« Feb</a></td><td class="pad">&nbsp;</td><td id="eo-widget-next-month" colspan="3"><a title="Mes próximo" href="http://yerbabuena?eo_month=2018-04"> Abr » </a></td></tr>
                                    </tfoot>
                                
                                </table>
                            </div>
                        </div>
                        </div><!-- /widget-calendar -->
                        
                        
                        
                        
                            
                            
                        

                    </section>
                    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>