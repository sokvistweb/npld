<?php 
$page = 'home';
?>

<?php include 'includes/header.php';?>
                    
                    
                    <?php include 'includes/i-map.php';?>
                    <?php include 'includes/map.php';?>
                    
                    
                    <!-- Banner -->
                    <section id="banner">
                        <div class="content">
                            <header>
                                <h1>Our languages in Europe</h1>
                            </header>
                            
                        </div>
                        <div class="image object">
                            <p>Aenean ornare velit lacus, ac varius enim ullamcorper eu. Proin aliquam facilisis ante interdum congue. Integer mollis, nisl amet convallis, porttitor magna ullamcorper, amet egestas mauris. Ut magna finibus nisi nec lacinia.</p>
                            <ul class="actions">
                                <li><a href="#" class="button big">Learn More</a></li>
                            </ul>
                        </div>
                    </section>
                    

                    <!-- Section -->
                    <section>
                        <div class="features">
                            <a href="page-our-members.html">
                                <article class="three-cols">
                                    <span class="icon"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-members"></use></svg></span>
                                    <div class="content">
                                        <h3>Members</h3>
                                        <p>Ac varius enim lorem ullamcorper dolore.</p>
                                    </div>
                                </article>
                            </a>
                            <a href="page.html">
                                <article class="three-cols">
                                    <span class="icon"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-actions"></use></svg></span>
                                    <div class="content">
                                        <h3>Actions & Activities</h3>
                                        <p>Sed nulla amet lorem feugiat.</p>
                                    </div>
                                </article>
                            </a>
                            <a href="page.html">
                                <article class="three-cols">
                                    <span class="icon"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-resources"></use></svg></span>
                                    <div class="content">
                                        <h3>Resources</h3>
                                        <p>Proin aliquam facilisis ante interdum.</p>
                                    </div>
                                </article>
                            </a>
                        </div>
                    </section>
                    

                    <!-- Section -->
                    <section>
                        <header class="major">
                            <h2>NPLD News</h2>
                        </header>
                        
                        <div class="row divider-flex posts">
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>The opening ceremony of Pompeu Fabra Year vindicates the Catalan as a language for everyone</h3>
                                        <p>Catalonia commemorates in 2018 the figure and the work of Pompeu Fabra on occasion of the 150 anniversary of his birth. This year also coincides with the 100th anniversary of the publication of Fabra's...</p>
                                        <div><a href="page-blog-single.html" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Nulla amet dolore ante interdum tempus aliquam</h3>
                                        <p>Sed nulla amet lorem feugiat tempus aliquam. Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum. Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore.</p>
                                        <div><a href="page-blog-single-2.html" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                    


<?php include 'includes/footer.php';?>