<?php 
$page = 'blog';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section>
                        <header class="main">
                            <h1>News</h1>
                        </header>

                        <div class="row divider-flex posts">
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Pompeu Fabra Year</h3>
                                        <p>Catalonia commemorates in 2018 the figure and the work of Pompeu Fabra on occasion of the 150 anniversary of his birth. This year also coincides with the 100th anniversary of the publication of Fabra's masterpiece Gramàtica Catalana.</p>
                                        <div><a href="page-blog-single.php" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Nulla amet dolore ante interdum tempus aliquam</h3>
                                        <p>Sed nulla amet lorem feugiat tempus aliquam. Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum.</p>
                                        <div><a href="page-blog-single-2.php" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Proin aliquam</h3>
                                        <p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet.</p>
                                        <div><a href="page-blog-single.php" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Blandit adipiscing integer semper</h3>
                                        <p>In arcu accumsan arcu adipiscing accumsan orci ac. Felis id enim aliquet. Accumsan ac integer lobortis commodo ornare aliquet accumsan erat tempus amet porttitor. Ante commodo orci eget.</p>
                                        <div><a href="page-blog-single.php" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Amet dolore ante interdum tempus aliquam</h3>
                                        <p>Sed nulla amet lorem feugiat tempus aliquam. Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum.</p>
                                        <div><a href="page-blog-single.php" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Interdum aenean facilisis</h3>
                                        <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis interdum. Sed nulla amet lorem feugiat tempus aliquam. Voluptate legam fore de summis. Cernantur ubi senserit.</p>
                                        <div><a href="page-blog-single.php" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                        </div>
                        
                        <ul class="pagination">
                            <li><span class="button disabled">Prev</span></li>
                            <li><a href="#" class="page active">1</a></li>
                            <li><a href="#" class="page">2</a></li>
                            <li><a href="#" class="page">3</a></li>
                            <li><span>&hellip;</span></li>
                            <li><a href="#" class="page">8</a></li>
                            <li><a href="#" class="page">9</a></li>
                            <li><a href="#" class="page">10</a></li>
                            <li><a href="#" class="button">Next</a></li>
                        </ul>

                    </section>
                    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>