<?php 
$page = 'page logged-in';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section>
                        <header class="main">
                            <h1>Calendar of activities</h1>
                        </header>
                        
                        
                        
                        <div class="full-calendar">


<div class="eo-fullcalendar eo-fullcalendar-shortcode eo-fullcalendar-reset eo-fullcalendar-responsive fc fc-ltr fc-unthemed" id="eo_fullcalendar_1">
    <div class="fc-toolbar">
        <div class="fc-left">
            <h2 aria-live="polite">May 2018</h2>
        </div>
        
        <div class="fc-right">
            <div class="fc-button-group">
                <button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right"><span class="fc-icon fc-icon-left-single-arrow"></span>
                </button>
            </div>
            <div class="fc-button-group">
                <button type="button" class="fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right"><span class="fc-icon fc-icon-right-single-arrow"></span>
                </button>
            </div>
            <div class="fc-button-group">
                <button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right">Today</button>
            </div>
        </div>
        
        <div class="fc-center"></div>
        
        <div class="fc-clear"></div>
    </div>
    
    <div class="fc-view-container" style="">
        <div class="fc-view fc-month-view fc-basic-view" style="">
            <table>
                <thead><tr><td class="fc-widget-header"><div class="fc-row fc-widget-header">
                    <table>
                        <thead>
                            <tr>
                                <th class="fc-day-header fc-widget-header fc-mon">Mon.</th>
                                <th class="fc-day-header fc-widget-header fc-tue">Tues.</th>
                                <th class="fc-day-header fc-widget-header fc-wed">Wed.</th>
                                <th class="fc-day-header fc-widget-header fc-thu">Thurs.</th>
                                <th class="fc-day-header fc-widget-header fc-fri">Fri.</th>
                                <th class="fc-day-header fc-widget-header fc-sat">Sat.</th>
                                <th class="fc-day-header fc-widget-header fc-sun">Sun.</th>
                            </tr>
                        </thead>
                    </table></div></td></tr>
                </thead>
                
                <tbody><tr><td class="fc-widget-content"><div class="fc-day-grid-container" style=""><div class="fc-day-grid"><div class="fc-row fc-week fc-widget-content"><div class="fc-bg">
                    <table><tbody><tr><td class="fc-day fc-widget-content fc-mon fc-other-month fc-past" data-date="2018-02-26"></td><td class="fc-day fc-widget-content fc-tue fc-other-month fc-past" data-date="2018-02-27"></td><td class="fc-day fc-widget-content fc-wed fc-other-month fc-past" data-date="2018-02-28"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2018-03-01"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2018-03-02"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2018-03-03"></td><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2018-03-04"></td></tr>
                        </tbody>
                    </table></div>
                    
                    <div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-mon fc-other-month fc-past" data-date="2018-02-26">26</td><td class="fc-day-number fc-tue fc-other-month fc-past" data-date="2018-02-27">27</td><td class="fc-day-number fc-wed fc-other-month fc-past" data-date="2018-02-28">28</td><td class="fc-day-number fc-thu fc-past" data-date="2018-03-01">1</td><td class="fc-day-number fc-fri fc-past" data-date="2018-03-02">2</td><td class="fc-day-number fc-sat fc-past" data-date="2018-03-03">3</td><td class="fc-day-number fc-sun fc-past" data-date="2018-03-04">4</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div>
                    
                    <div class="fc-row fc-week fc-widget-content"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2018-03-05"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2018-03-06"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2018-03-07"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2018-03-08"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2018-03-09"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2018-03-10"></td><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2018-03-11"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-mon fc-past" data-date="2018-03-05">5</td><td class="fc-day-number fc-tue fc-past" data-date="2018-03-06">6</td><td class="fc-day-number fc-wed fc-past" data-date="2018-03-07">7</td><td class="fc-day-number fc-thu fc-past" data-date="2018-03-08">8</td><td class="fc-day-number fc-fri fc-past" data-date="2018-03-09">9</td><td class="fc-day-number fc-sat fc-past" data-date="2018-03-10">10</td><td class="fc-day-number fc-sun fc-past" data-date="2018-03-11">11</td></tr></thead><tbody><tr><td></td><td></td><td class="fc-event-container">
                        <a class="fc-day-grid-event fc-event fc-start fc-end eo-event-venue-yerbabuena-espai eo-event-cat-taller eo-event-past eo-event eo-past-event venue-yerbabuena-espai category-taller" href="page-calendar-single.php" style="background-color:#1e8cbe;border-color:#1e8cbe;color:#ffffff" data-hasqtip="83">
                        <div class="fc-content"><span class="eo-fullcalendar-screen-reader-text screen-reader-text"> Thursday, 21 February 2018 </span><span class="fc-time">9:30 am</span> <span class="fc-title">NPLD Annual Conference</span>
                        </div>
                        </a></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div>
                    
                    <div class="fc-row fc-week fc-widget-content"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2018-03-12"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2018-03-13"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2018-03-14"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2018-03-15"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2018-03-16"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2018-03-17"></td><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2018-03-18"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-mon fc-past" data-date="2018-03-12">12</td><td class="fc-day-number fc-tue fc-past" data-date="2018-03-13">13</td><td class="fc-day-number fc-wed fc-past" data-date="2018-03-14">14</td><td class="fc-day-number fc-thu fc-past" data-date="2018-03-15">15</td><td class="fc-day-number fc-fri fc-past" data-date="2018-03-16">16</td><td class="fc-day-number fc-sat fc-past" data-date="2018-03-17">17</td><td class="fc-day-number fc-sun fc-past" data-date="2018-03-18">18</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div>
                    
                    <div class="fc-row fc-week fc-widget-content"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2018-03-19"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2018-03-20"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2018-03-21"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2018-03-22"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2018-03-23"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2018-03-24"></td><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2018-03-25"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-mon fc-past" data-date="2018-03-19">19</td><td class="fc-day-number fc-tue fc-past" data-date="2018-03-20">20</td><td class="fc-day-number fc-wed fc-past" data-date="2018-03-21">21</td><td class="fc-day-number fc-thu fc-past" data-date="2018-03-22">22</td><td class="fc-day-number fc-fri fc-past" data-date="2018-03-23">23</td><td class="fc-day-number fc-sat fc-past" data-date="2018-03-24">24</td><td class="fc-day-number fc-sun fc-past" data-date="2018-03-25">25</td></tr></thead><tbody><tr><td rowspan="3"></td><td rowspan="3"></td>
                        <td class="fc-event-container"><a class="fc-day-grid-event fc-event fc-start fc-not-end eo-event-venue-yerbabuena-espai eo-event-cat-consulta eo-event-tag-consulta eo-event-tag-estado-de-animo eo-event-tag-psicologia eo-event-tag-salud-mental eo-event-tag-salud-natural eo-event-running eo-multi-day eo-event eo-past-event venue-yerbabuena-espai category-consulta tag-consulta tag-estado-de-animo tag-psicologia tag-salud-mental tag-salud-natural" href="page-calendar-single.php" style="background-color:#1e8cbe;border-color:#1e8cbe;color:#ffffff" data-hasqtip="84"><div class="fc-content"><span class="eo-fullcalendar-screen-reader-text screen-reader-text"> miércoles, marzo 21st 2018 </span><span class="fc-time">3:00 pm</span> <span class="fc-title">Languages and the Economy</span></div></a>
                        </td></tr><tr><td rowspan="2"></td><td rowspan="2"></td><td rowspan="2"></td><td rowspan="2"></td></tr><tr></tr></tbody></table></div></div>
                    
                    <div class="fc-row fc-week fc-widget-content"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2018-03-26"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2018-03-27"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2018-03-28"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2018-03-29"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2018-03-30"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2018-03-31"></td><td class="fc-day fc-widget-content fc-sun fc-other-month fc-past" data-date="2018-04-01"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-mon fc-past" data-date="2018-03-26">26</td><td class="fc-day-number fc-tue fc-past" data-date="2018-03-27">27</td><td class="fc-day-number fc-wed fc-past" data-date="2018-03-28">28</td><td class="fc-day-number fc-thu fc-past" data-date="2018-03-29">29</td><td class="fc-day-number fc-fri fc-past" data-date="2018-03-30">30</td><td class="fc-day-number fc-sat fc-past" data-date="2018-03-31">31</td><td class="fc-day-number fc-sun fc-other-month fc-past" data-date="2018-04-01">1</td></tr></thead>
                        
                        <tbody><tr>
                            <td class="fc-event-container"><a class="fc-day-grid-event fc-event fc-not-start fc-not-end eo-event-venue-yerbabuena-espai eo-event-cat-consulta eo-event-tag-consulta eo-event-tag-estado-de-animo eo-event-tag-psicologia eo-event-tag-salud-mental eo-event-tag-salud-natural eo-event-running eo-multi-day eo-event eo-past-event venue-yerbabuena-espai category-consulta tag-consulta tag-estado-de-animo tag-psicologia tag-salud-mental tag-salud-natural" href="page-calendar-single.php" style="background-color:#1e8cbe;border-color:#1e8cbe;color:#ffffff" data-hasqtip="85"><div class="fc-content"><span class="eo-fullcalendar-screen-reader-text screen-reader-text"> miércoles, marzo 21st 2018 </span> <span class="fc-time">10:00 am</span><span class="fc-title"> Language and Mobility/Migration Invitation</span></div></a>
                            </td></tr><tr>
                            <td rowspan="2"></td><td rowspan="2"></td>
                            
                            </tr><tr><td></td>
                            
                            <td class="fc-event-container"><a class="fc-day-grid-event fc-event fc-start fc-not-end eo-event-venue-yerbabuena-espai eo-event-cat-taller eo-event-running eo-multi-day eo-event eo-past-event venue-yerbabuena-espai category-taller" href="page-calendar-single.php" style="background-color:#1e8cbe;border-color:#1e8cbe;color:#ffffff" data-hasqtip="89"><div class="fc-content"><span class="eo-fullcalendar-screen-reader-text screen-reader-text"> jueves, marzo 29th 2018 </span><span class="fc-time">7:30 pm</span> <span class="fc-title">Voices in Artes</span></div></a>
                            </td></tr>
                        </tbody></table></div></div></div></div></td></tr></tbody>
            </table>
        </div>
    </div>
</div><!-- /eo-fullcalendar -->


                      
                        </div><!-- /full-calendar -->
                            
                            
                        

                    </section>
                    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>