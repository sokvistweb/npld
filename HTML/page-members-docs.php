<?php 
$page = 'page logged-in';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section>
                        <header class="main">
                            <h1>Other NPLD documents for members</h1>
                        </header>

                        <ul class="docs alt">
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-pdf"></use></svg><span>EU_Grant_Application.pdf</span><span>13 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-word"></use></svg><span>1.1a Financial Capacity EN.docx</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-pdf"></use></svg><span>EU_Grant_offer_with_conditions.pdf</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-pdf"></use></svg><span>EU_Grant_agreed_buget.pdf</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-pdf"></use></svg><span>EU_Formal_grant_agreement.pdf</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-pdf"></use></svg><span>Financial Management.pdf</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-word"></use></svg><span>EU Grant Documentation.docx</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-word"></use></svg><span>Grant offer signet.docx</span><span>12 Dec 2013</span></a></li>
                            <li class="cat-item cat-item-1"><a href="#"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-file-excel"></use></svg><span>EU Grant Monitoring and Evaluations.xlsm</span><span>12 Dec 2013</span></a></li>
                        </ul>
                        
                        <ul class="pagination">
                            <li><span class="button disabled">Prev</span></li>
                            <li><a href="#" class="page active">1</a></li>
                            <li><a href="#" class="page">2</a></li>
                            <li><a href="#" class="page">3</a></li>
                            <li><span>&hellip;</span></li>
                            <li><a href="#" class="page">8</a></li>
                            <li><a href="#" class="page">9</a></li>
                            <li><a href="#" class="page">10</a></li>
                            <li><a href="#" class="button">Next</a></li>
                        </ul>

                    </section>
                    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>