<?php 
$page = 'single';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section class="blog-single">
                        <header class="main">
                            <h1>Pompeu Fabra Year</h1>
                        </header>

                        <div class="row">
                            <div class="w12u">
                                <article>

                                    <h2>The opening ceremony of Pompeu Fabra Year vindicates the Catalan as a language for everyone</h2>
                                    
                                    <p>Catalonia commemorates in 2018 the figure and the work of Pompeu Fabra on occasion of the 150 anniversary of his birth. This year also coincides with the 100th anniversary of the publication of Fabra's masterpiece Gramàtica Catalana.</p>
                                    
                                    <blockquote>The General Director of Linguistic Policy of Catalonia's Government, Ester Franquesa, emphasized that "we have initiated the Fabra Year with the will to remember and to serve as a stimulus to socially strengthen Catalan. As Fabra did, we maintain the aspiration of fulfilment of the Catalan language. Let Catalan be the language of everything and everyone".</blockquote>
                                    
                                    <p>The ceremony was held on 21 March at Zorrilla Theater in Badalona (Catalonia) and was attended by 450 participants. The Governments of València and Balearic Islands were represented at the event by Rubén Trenzando and Marta Fuxà, respectively.</p>
                                    
                                    <p>Pompeu Fabra (1868 - 1948) is known for having established the modern grammar of the Catalan language</p>
                                    
                                    <p>Click <a href="http://llengua.gencat.cat/ca/detalls/noticia/Lacte-inaugural-de-lAny-Pompeu-Fabra-reivindica-el-catala-com-a-llengua-per-a-tothom">here for + info</a> in Catalan and here, in English</p>
                                    
                                    <p>Follow <a href="https://twitter.com/AnyFabra">#AnyFabra</a> on Twitter</p>

                                </article>
                            </div>
                        </div>

                    </section>
                    <!-- /Content -->
                    
                    
                    <section>
                        <header class="major">
                            <h2>More News</h2>
                        </header>
                        <div class="row divider-flex posts">
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Nulla amet dolore ante interdum tempus aliquam</h3>
                                        <p>Sed nulla amet lorem feugiat tempus aliquam. Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum.</p>
                                        <div><a href="page-blog-single-2.html" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                            <div class="w6u equal-h">
                                <article>
                                    <div class="box-style">
                                        <h3>Proin aliquam</h3>
                                        <p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet.</p>
                                        <div><a href="page-blog-single.html" class="button special">Read more</a></div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                    
                    
<?php include 'includes/footer.php';?>