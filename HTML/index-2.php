<?php include 'includes/header.php';?>

                    
                    <!-- Banner -->
                    <section id="banner">
                        <div class="content">
                            <header>
                                <h1>Our languages in Europe</h1>
                            </header>
                            
                        </div>
                        <div class="image object">
                            <h2>Ut magna finibus nisi nec lacinia</h2>
                            <p>Aenean ornare velit lacus, ac varius enim ullamcorper eu. Proin aliquam facilisis ante interdum congue. Integer mollis, nisl amet convallis, porttitor magna ullamcorper, amet egestas mauris. Ut magna finibus nisi nec lacinia.</p>
                            <ul class="actions">
                                <li><a href="#" class="button big">Learn More</a></li>
                            </ul>
                        </div>
                    </section>
                    
                    
                    <!-- Section -->
                    <section class="i-map">
                        <div class="interactive-map">
                            <img src="assets/images/europe-map.png">

                            <button id="alsatian" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Alsatian</h2>
                                        <p>France/Germany</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="basque" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Basque</h2>
                                        <p>Navarre & Basque Country -Spain/France</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="breton" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Breton</h2>
                                        <p>France</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="catalan" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Catalan</h2>
                                        <p>Catalonia, Valencia and Balearic Islands</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="cornish" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Cornish</h2>
                                        <p>UK</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="corsican" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Corsican</h2>
                                        <p>France</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="finnish" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Finnish</h2>
                                        <p>Sweden</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="frisian" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Frisian</h2>
                                        <p>Netherlands</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="friulian" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Friulian</h2>
                                        <p>Venice, Italy</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="galician" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Galician</h2>
                                        <p>Spain</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="irish-gaelic" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Irish Gaelic</h2>
                                        <p>Ireland</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="ladin" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Ladin, Mocheni, Cimbri</h2>
                                        <p>Trento, Italy</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="macedonian" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Macedonian</h2>
                                        <p>Greece</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="nynorsk" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Nynorsk</h2>
                                        <p>Norway</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="occitan" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Occitan</h2>
                                        <p>France</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="scottish-gaelic" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Scottish Gaelic</h2>
                                        <p>Norway</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="swedish" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Swedish</h2>
                                        <p>Finland</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="welsh" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Welsh</h2>
                                        <p>UK</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="kashubian" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Kashubian</h2>
                                        <p>Poland</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                            <button id="hungarian" class="map-point">
                                <div class="content">
                                    <div class="centered-y">
                                        <h2>Hungarian</h2>
                                        <p>Romania</p>
                                        <a class="button special small" href="#" title="More info">More info</a>
                                    </div>
                                </div>
                            </button>
                        </div>
                    </section>
                    

                    <!-- Section -->
                    <section>
                        <div class="features">
                            <article class="three-cols">
                                <span class="icon fa-users"></span>
                                <div class="content">
                                    <h3>Members</h3>
                                    <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore.</p>
                                </div>
                            </article>
                            <article class="three-cols">
                                <span class="icon fa-paper-plane"></span>
                                <div class="content">
                                    <h3>Actions & Activities</h3>
                                    <p>Sed nulla amet lorem feugiat tempus aliquam.</p>
                                </div>
                            </article>
                            <article class="three-cols">
                                <span class="icon fa-bullseye"></span>
                                <div class="content">
                                    <h3>Resources</h3>
                                    <p>Proin aliquam facilisis ante interdum.</p>
                                </div>
                            </article>
                        </div>
                    </section>
                    

                    <!-- Section -->
                    <section>
                        <header class="major">
                            <h2>NPLD News</h2>
                        </header>
                        <div class="posts">
                            <article>
                                <div class="article-content">
                                    <h3>Interdum aenean facilisis</h3>
                                    <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis interdum. Sed nulla amet lorem feugiat tempus aliquam. Voluptate legam fore de summis. Cernantur ubi senserit.</p>
                                    <ul class="actions">
                                        <li><a href="#" class="button">More</a></li>
                                    </ul>
                                </div>
                            </article>
                            <article>
                                <div class="article-content">
                                    <h3>Nulla amet dolore ante interdum tempus aliquam</h3>
                                    <p>Sed nulla amet lorem feugiat tempus aliquam. Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin aliquam facilisis ante interdum.</p>
                                    <ul class="actions">
                                        <li><a href="#" class="button">More</a></li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                    </section>
                    
                    
<?php include 'includes/footer.php';?>