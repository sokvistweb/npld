                <footer id="footer">

                    <!-- Section -->
                    <div class="getintouch">
                        <header class="major">
                            <h2>Get in touch</h2>
                        </header>
                        <p>Sed varius enim lorem ullamcorper dolore aliquam aenean ornare velit lacus.</p>
                        <ul class="contact">
                            <li class="fa-envelope-o"><a href="#">info@npld.eu</a></li>
                            <li class="fa-phone"><a href="#">+00 000 0000</a></li>
                            <li class="fa-home">Diamantstraat 254,<br />
                                0000 Brussels, Belgium</li>
                        </ul>
                    </div>

                    <!-- MailChimp -->
                    <div id="mc_embed_signup" class="subscribe-form">
                        <header class="major">
                            <h2>Subcribe to our newsletter</h2>
                        </header>
                        <form action="//sokvist.us9.list-manage.com/subscribe/post?u=e5b778581d428a50593d91c5f&amp;id=e98b7563d8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                            <div id="mc_embed_signup_scroll">

                            <div class="mc-field-group">
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                            </div>
                            <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                                                                    </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                            <div class="clear">
                            <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                            </div>
                        </form>
                    </div>

                </footer>

            </div><!-- /.inner -->
        </div><!-- /Main -->


        <!-- Sidebar -->
        <div id="sidebar">
            <div class="inner">

                <!-- Login -->
                <section class="login-top">
                    <ul class="actions vertical">
                        <li><a href="#" class="button fit">Members Log in</a></li>
                    </ul>
                </section>

                <!-- Menu -->
                <nav id="menu">
                    <ul>
                        <li><a href="index.php">Homepage</a></li>
                        <li>
                            <span class="opener">About us</span>
                            <ul>
                                <li><a href="page.php">About NPLD</a></li>
                                <li><a href="page.php">Objectives</a></li>
                                <li><a href="page.php">Structure </a></li>
                            </ul>
                        </li>
                        <li><a href="page-our-members.php">Our Members</a></li>
                        <li><a href="page-our-languages.php">Our Languages</a></li>
                        <li>
                            <span class="opener">Our actions &amp; activities</span>
                            <ul>
                                <li><a href="page.php">Advocacy at EU and national level</a></li>
                                <li><a href="page.php">Exchange of best practices &amp; Network meetings</a></li>
                                <li><a href="page.php">NPLD projects</a></li>
                            </ul>
                        </li>
                        <li><a href="page.php">Funding</a></li>
                        <li><a href="page.php">Resources on linguistic diversity in Europe</a></li>
                        <li><a href="page-blog.php">News </a></li>
                        <li><a href="page.php">Newsletter</a></li>
                        <li><a href="page.php">Become a member</a></li>
                        <li><a href="page-contact.php">Contact us</a></li>
                        <li>
                            <span class="opener">Members’ section</span>
                            <ul>
                                <li><a href="page-calendar.php">Calendar of NPLD activities</a></li>
                                <li><a href="page-members-section.php">NPLD Statutory documents</a></li>
                                <li><a href="page-members-section.php">NPLD meetings</a></li>
                                <li><a href="page-members-section.php">Financial issues</a></li>
                                <li><a href="page-members-docs.php">Other NPLD documents for members</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>


                <!-- Section -->
                <section>
                    <header class="major">
                        <h2>Ante interdum</h2>
                    </header>
                    <div class="mini-posts">
                        <article>
                            <a href="#" class="image"><img src="assets/images/melt-logo.png" alt="" /></a>
                            <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
                        </article>
                    </div>
                    <ul class="actions">
                        <li><a href="http://www.npld.eu/melt/" target="_blank" class="button">More</a></li>
                    </ul>
                </section>

                <!-- Search -->
                <section id="search">
                    <form method="post" action="#">
                        <input type="text" name="query" id="query" placeholder="Search" />
                    </form>
                </section>

                <!-- Section -->
                <section class="alt">
                    <p class="copyright">&copy; NPLD 2018. Design: <a href="https://sokvist.com">Sokvist</a>.</p>
                </section>

            </div><!-- /.inner -->
        </div><!-- /Sidebar -->

    </div><!-- /Wrapper -->


    <!-- Scripts -->
    <script src="assets/js/vendor/jquery.min.js"></script>
    <script src="assets/js/min/plugins.min.js"></script>
    <script src="assets/js/min/main.min.js"></script>
        

	</body>
</html>