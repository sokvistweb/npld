<!-- Section -->
<section class="map">
    <div class="simple-map">
        <img src="assets/images/europe-map-simple.svg">
        <ul>
            <li id="alsatian" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Alsatian</h2>
                        <p>France/Germany</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="basque" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Basque</h2>
                        <p>Navarre & Basque Country -Spain/France</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="breton" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Breton</h2>
                        <p>France</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="catalan" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Catalan</h2>
                        <p>Catalonia, Valencia and Balearic Islands</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="cornish" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Cornish</h2>
                        <p>UK</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="corsican" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Corsican</h2>
                        <p>France</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="finnish" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Finnish</h2>
                        <p>Sweden</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="frisian" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Frisian</h2>
                        <p>Netherlands</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="friulian" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Friulian</h2>
                        <p>Venice, Italy</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="galician" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Galician</h2>
                        <p>Spain</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="irish-gaelic" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Irish Gaelic</h2>
                        <p>Ireland</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="ladin" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Ladin, Mocheni, Cimbri</h2>
                        <p>Trento, Italy</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="macedonian" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Macedonian</h2>
                        <p>Greece</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="nynorsk" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Nynorsk</h2>
                        <p>Norway</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="occitan" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Occitan</h2>
                        <p>France</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="scottish-gaelic" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Scottish Gaelic</h2>
                        <p>Norway</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="swedish" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Swedish</h2>
                        <p>Finland</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="welsh" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Welsh</h2>
                        <p>UK</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="kashubian" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Kashubian</h2>
                        <p>Poland</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
            <li id="hungarian" class="map-list">
                <div class="content">
                    <div class="centered-y">
                        <h2>Hungarian</h2>
                        <p>Romania</p>
                        <a class="button special small" href="#" title="More info">More info</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <!-- /end of simple-map -->
</section>