<!DOCTYPE HTML>
<html lang="en">
	<head>
	    <meta charset="utf-8" />
		<title>NPLD | Our languages in Europe</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="sokvist.com">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
       
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png" />
        <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
        <link rel="stylesheet" href="assets/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Hind:400,700|Open+Sans:400,400i,600" rel="stylesheet">

	</head>
	<body class="<?php echo $page; ?>">

		<!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <a href="index.php" class="NPLD logo"><img src="assets/images/npld-logo.svg" alt="NPLD logo" width="268" height="150" /></a>
                        <ul class="icons">
                            <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                            <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                            <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                            <li><a href="#" class="icon fa-phone"><span class="label">Phone</span></a></li>
                        </ul>
                    </header>