<!-- Section -->
<section class="i-map">
    <div class="interactive-map">
        <img src="assets/images/europe-map.svg">

        <button id="alsatian" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Alsatian</h2>
                    <p>France/Germany</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="basque" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Basque</h2>
                    <p>Navarre & Basque Country -Spain/France</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="breton" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Breton</h2>
                    <p>France</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="catalan" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Catalan</h2>
                    <p>Catalonia, Valencia and Balearic Islands</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="cornish" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Cornish</h2>
                    <p>UK</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="corsican" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Corsican</h2>
                    <p>France</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="finnish" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Finnish</h2>
                    <p>Sweden</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="frisian" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Frisian</h2>
                    <p>Netherlands</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="friulian" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Friulian</h2>
                    <p>Venice, Italy</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="galician" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Galician</h2>
                    <p>Spain</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="irish-gaelic" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Irish Gaelic</h2>
                    <p>Ireland</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="ladin" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Ladin, Mocheni, Cimbri</h2>
                    <p>Trento, Italy</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="macedonian" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Macedonian</h2>
                    <p>Greece</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="nynorsk" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Nynorsk</h2>
                    <p>Norway</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="occitan" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Occitan</h2>
                    <p>France</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="scottish-gaelic" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Scottish Gaelic</h2>
                    <p>Norway</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="swedish" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Swedish</h2>
                    <p>Finland</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="welsh" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Welsh</h2>
                    <p>UK</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="kashubian" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Kashubian</h2>
                    <p>Poland</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
        <button id="hungarian" class="map-point">
            <div class="content">
                <div class="centered-y">
                    <h2>Hungarian</h2>
                    <p>Romania</p>
                    <a class="button special small" href="#" title="More info">More info</a>
                </div>
            </div>
        </button>
    </div>
    <!-- /end of interactive-map -->
</section>