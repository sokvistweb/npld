<?php 
$page = 'page';
?>

<?php include 'includes/header.php';?>

                    
    <!-- Content -->
    <section>
        <header class="main">
            <h1>Our Members</h1>
        </header>


        <div class="row">
            <div class="w12u">
                <article>

                    <p>The Network comprises of two levels of membership - full members and associate members. The full members are the main funders of the network and automatically have a seat on the Steering Committee. The associate members pay a lesser fee and are represented on the Steering Committee by six associate members who are chosen by the associate members themselves. All the associate members can participate and discuss issues in the Network by attending the annual General Assembly. All members will be expected to contribute towards the funding of the Network.</p>

                    <p>Full members have greater voting powers within the Network.s structure which reflects their greater financial contribution. All Steering Committee members, however, will have an influence on policy decisions and they will also be able to decide on who can be proposed as new members of the network. The Steering Committee will also make the final decision regarding the levels of fees to be paid by members. All members will be able to participate in the Network.s projects and contribute to the Network's overall policy framework.</p>

                    <p>A list of all current members, full and associate, is available below. Click on the links for more details (Organisations marked with a * are associate members that are currently elected to sit on the Steering Committee.)</p>

                </article>
            </div>
        </div>


        <div class="row">
            <div class="w6u">
                <h2>Full Members</h2>
                <ul class="alt docs">
                    <li><a href="page-our-members-single.html">Folktinget: Svenska Finlands folkting</a></li>
                    <li><a href="page-our-members-single.html">Ofis Publik ar Brezhoneg / Public Office for the Breton Language</a></li>
                    <li><a href="page-our-members-single.html">Cullettività Territuriale di Corsica / Territorial Collectivity of Corsica</a></li>
                    <li><a href="page-our-members-single.html">Government of Galicia</a></li>
                    <li><a href="page-our-members-single.html">Government of Catalonia</a></li>
                    <li><a href="page-our-members-single.html">Province of Friesland</a></li>
                    <li><a href="page-our-members-single.html">Government of Wales</a></li>
                    <li><a href="page-our-members-single.html">Bizkaiko Foru Aldundia / Bizkaia Provincial Council</a></li>
                    <li><a href="page-our-members-single.html">Government of Navarre</a></li>
                    <li><a href="page-our-members-single.html">Government of Ireland &amp; Foras na Gaeilge</a></li>
                    <li><a href="page-our-members-single.html">Government of the Basque Country</a></li>
                    <li><a href="page-our-members-single.html">Province of Trento - Fassa Valley (Fascia) - Mocheni Valley (Bersentol) - Luserna (Lusern)</a></li>
                    <li><a href="page-our-members-single.html">Friuli Venezia Giulia Regional Council/ Regional Agency for Friulian language</a></li>
                    <li><a href="page-our-members-single.html">Government of Balearic Islands</a></li>
                    <li><a href="page-our-members-single.html">Government of Valencia</a></li>
                </ul>
            </div>

            <div class="w6u">
                <h2>Associate Members</h2>
                <ul class="alt docs">
                    <li class="first"><a href="page-our-members-single.html">Fryske Akademy*</a></li>
                    <li><a href="page-our-members-single.html">Prifysgol Bangor / Bangor University</a></li>
                    <li><a href="page-our-members-single.html">Cornwall Council</a></li>
                    <li><a href="page-our-members-single.html">Europeesk Buro Foar Lytsen Talen (EBLT) / European Bureau for Lesser Used Languages</a></li>
                    <li><a href="page-our-members-single.html">Coleg Cymraeg Cenedlaethol* / Welsh National College</a></li>
                    <li><a href="page-our-members-single.html">Aurten Bai Foundation*</a></li>
                    <li><a href="page-our-members-single.html">Prifysgol Caerdydd / Cardiff University</a></li>
                    <li><a href="page-our-members-single.html">Noregs Mallag</a></li>
                    <li><a href="page-our-members-single.html">Institut d'Estudis Occitans</a></li>
                    <li><a href="page-our-members-single.html">Soillse &amp; The University of the Highlands and Islands*</a></li>
                    <li><a href="page-our-members-single.html">Stockholm University*</a></li>
                    <li><a href="page-our-members-single.html">The Archives of the Sweden Finns</a></li>
                    <li><a href="page-our-members-single.html">Conseil départemental du Finistère</a></li>
                    <li><a href="page-our-members-single.html">Soziolinguistika Klusterra</a></li>
                    <li><a href="page-our-members-single.html">Convergéncia Occitana*</a></li>
                    <li><a href="page-our-members-single.html">University of Wales Trinity Saint David* / Prifysgol Cymru Y Drindod Dewi Sant </a></li>
                    <li><a href="page-our-members-single.html">Mentrau Iaith Cymru / Welsh Language Initiatives</a></li>
                    <li><a href="page-our-members-single.html">Plataforma per la Llengua / Platform for the Language</a></li>
                    <li><a href="page-our-members-single.html">Office pour la langue et les cultures d'Alsace et de Moselle (OLCA)</a></li>
                    <li><a href="page-our-members-single.html">Puck Regional Association</a></li>
                    <li><a href="page-our-members-single.html">Universitatea Creștină Partium / Partium Christian University / Partiumi Keresztény Egyetem </a></li>
                </ul>
            </div>
        </div>


    </section>
    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>