<?php 
$page = 'page logged-in';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section>
                        <header class="main">
                            <h1>NPLD Statutory documents</h1>
                        </header>

                        <ul class="docs alt">
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>NPLD Constitutional documents</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>Chair's Committee meetings</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>General Assembly</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>NPLD meetings in Fassa Valley, June 2017</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>Approved Minutes of NPLD Meetings</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>NPLD Commissioned Reports</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>Financial Management</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>2013-2015 EU Grant Documentation</span></a></li>
                            <li class="cat-item cat-item-1"><a href="page-members-docs.php"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-folder"></use></svg><span>2013-2015 EU Grant Monitoring and Evaluations Documentation </span></a></li>
                        </ul>

                    </section>
                    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>