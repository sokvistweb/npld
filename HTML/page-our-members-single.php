<?php 
$page = 'page';
?>

<?php include 'includes/header.php';?>

                    
                    <!-- Content -->
                    <section>
                        <header class="main">
                            <h1>Folktinget: Svenska Finlands folkting</h1>
                        </header>

                        <div class="card-full">
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Name of organisation</p>
                                </div>
                                <div class="card-col with-img">
                                    <p>Svenska Finlands folkting</p>
                                    <img src="assets/images/folktinget-logo.png" alt="">
                                </div>
                            </div>
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Country</p>
                                </div>
                                <div class="card-col">
                                    <p>Finland</p>
                                </div>
                            </div>
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Type of Organisation</p>
                                </div>
                                <div class="card-col">
                                    <p>Full Member. Public body</p>
                                </div>
                            </div>
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Short note on organisation background</p>
                                </div>
                                <div class="card-col">
                                    <p>Folktinget is a statutory organization founded in 1919, whose tasks are laid down by law. Finnish legislation guarantees that the Swedish Assembly and its activities are supported in the state budget.</p>

                                    <p>The task of Folktinget is to safeguard the Swedish language and the interests of the Swedish-speaking population in Finland. Folktinget participates actively in the law-drafting process and constantly issues statements to governmental and municipal institutions on issues and reform plans concerning the Swedish-speaking population. Folktinget is a cross-political body; all parliamentary parties with activities in Swedish are members of Folktinget.</p>

                                    <p>As part of its work, Folktinget receives reports and complaints from the public about cases where citizens feel that their linguistic rights in Swedish have not been fulfilled. Folktinget looks into the cases and actively contacts the authorities concerned, with statements and consultation on how services in Swedish should be provided according to the Language Act and other relevant legislation.</p>

                                    <p>Folktinget also publishes results from inquiries, distributes brochures on linguistic rights, and disseminates information on Swedish culture in Finland. In addition, the Swedish Assembly compiles information and statistical reports on the Swedish-speaking Finns, and works actively to promote positive attitudes towards bilingualism through campaigns and other events.</p>

                                    <p>Folktinget has, since the 1940s, arranged the main Swedish Day celebrations. Swedish Day is celebrated on 6 November, and symbolizes the right for Swedish-speaking Finns to use their mother tongue. It is also an occasion for celebrating Finland as a bilingual nation.</p>
                                </div>
                            </div>
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Website</p>
                                </div>
                                <div class="card-col">
                                    <p><a href="https://folktinget.fi/sv/start/" target="_blank">https://folktinget.fi/sv/start/</a></p>
                                </div>
                            </div>
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Contact</p>
                                </div>
                                <div class="card-col">
                                    <p>Markus Österlund, Secretary General of Folktinget</p>

                                    <p>Tel: +358-9-6844 250 / +358-9-6844 250</p>

                                    <p>International Coordinator +358-9-6844 2553 / +358-9-6844 2553</p>

                                    <p>Information on the Finnish Language Act, setting out the principles for services in both national languages, Finnish and Swedish:
                                    <a href="http://www.om.fi/en/Etusivu/Perussaannoksia/Kielilaki" target="_blank">http://www.om.fi/en/Etusivu/Perussaannoksia/Kielilaki</a></p>
                                </div>
                            </div>
                            <div class="card-row">
                                <div class="card-col">
                                    <p>Address</p>
                                </div>
                                <div class="card-col">
                                    <p>The Swedish Assembly of Finland<br>
                                        Snellmansgatan 13 A,00170 HELSINKI, FINLAND</p>
                                </div>
                            </div>
                        </div>

                    </section>
                    <!-- /Content -->
                    
                    
<?php include 'includes/footer.php';?>